$(document).ready(function () {

    //Цели для Яндекс.Метрики и Google Analytics
    $(".count_element").on("click", (function () {
        ga("send", "event", "goal", "goal");
        yaCounterXXXXXXXX.reachGoal("goal");
        return true;
    }));

    //SVG Fallback
    if (!Modernizr.svg) {
        $("img[src*='svg']").attr("src", function () {
            return $(this).attr("src").replace(".svg", ".png");
        });
    };

    //Аякс отправка форм
    //Документация: http://api.jquery.com/jquery.ajax/
    $("#form").submit(function () {
        $.ajax({
            type: "POST",
            url: "mail.php",
            data: $(this).serialize()
        }).done(function () {
            alert("Спасибо за заявку!");
            setTimeout(function () {

                $("#form").trigger("reset");
            }, 1000);
        });
        return false;
    });

    //Chrome Smooth Scroll
    try {
        $.browserSelector();
        if ($("html").hasClass("chrome")) {
            $.smoothScroll();
        }
    } catch (err) {

    };

    $("img, a").on("dragstart", function (event) {
        event.preventDefault();
    });

    $('#map-hide').click(function(){
        $('#map-hide').hide();
    });

    $("#vacancy-more").click(function(){
        $("#page-vacancy .container-vacancy").append('<div class="page-items"><div class="item-left-part"><h4 class="item-title"><a href="#">Менеджер по туризму</a></h4><p class="company"><span>Компания:</span><a href="#">Поехали с нами</a></p><p class="item-description">Лидер в туризме — Сеть туристических агентств «Поехали с нами» проводит, на конкурсной основе, собеседования…</p><p class="city"><a href="#">Москва</a></p></div><div class="item-right-part"><h4 class="price">30 000–55 000 Р</h4><p class="time">3 минуты назад</p><a href="#" class="add-favorite"><span class="glyphicon glyphicon-bookmark"></span>В закладки</a></div></div>');
        $("#page-vacancy .container-vacancy").append('<div class="page-items"><div class="item-left-part"><h4 class="item-title"><a href="#">Менеджер по туризму</a></h4><p class="company"><span>Компания:</span><a href="#">Поехали с нами</a></p><p class="item-description">Лидер в туризме — Сеть туристических агентств «Поехали с нами» проводит, на конкурсной основе, собеседования…</p><p class="city"><a href="#">Москва</a></p></div><div class="item-right-part"><h4 class="price">30 000–55 000 Р</h4><p class="time">3 минуты назад</p><a href="#" class="add-favorite"><span class="glyphicon glyphicon-bookmark"></span>В закладки</a></div></div>');
        $("#page-vacancy .container-vacancy").append('<div class="page-items"><div class="item-left-part"><h4 class="item-title"><a href="#">Менеджер по туризму</a></h4><p class="company"><span>Компания:</span><a href="#">Поехали с нами</a></p><p class="item-description">Лидер в туризме — Сеть туристических агентств «Поехали с нами» проводит, на конкурсной основе, собеседования…</p><p class="city"><a href="#">Москва</a></p></div><div class="item-right-part"><h4 class="price">30 000–55 000 Р</h4><p class="time">3 минуты назад</p><a href="#" class="add-favorite"><span class="glyphicon glyphicon-bookmark"></span>В закладки</a></div></div>');
        $("#page-vacancy .container-vacancy").append('<div class="page-items"><div class="item-left-part"><h4 class="item-title"><a href="#">Менеджер по туризму</a></h4><p class="company"><span>Компания:</span><a href="#">Поехали с нами</a></p><p class="item-description">Лидер в туризме — Сеть туристических агентств «Поехали с нами» проводит, на конкурсной основе, собеседования…</p><p class="city"><a href="#">Москва</a></p></div><div class="item-right-part"><h4 class="price">30 000–55 000 Р</h4><p class="time">3 минуты назад</p><a href="#" class="add-favorite"><span class="glyphicon glyphicon-bookmark"></span>В закладки</a></div></div>');
        $("#page-vacancy .container-vacancy").append('<div class="page-items"><div class="item-left-part"><h4 class="item-title"><a href="#">Менеджер по туризму</a></h4><p class="company"><span>Компания:</span><a href="#">Поехали с нами</a></p><p class="item-description">Лидер в туризме — Сеть туристических агентств «Поехали с нами» проводит, на конкурсной основе, собеседования…</p><p class="city"><a href="#">Москва</a></p></div><div class="item-right-part"><h4 class="price">30 000–55 000 Р</h4><p class="time">3 минуты назад</p><a href="#" class="add-favorite"><span class="glyphicon glyphicon-bookmark"></span>В закладки</a></div></div>');
    });

    $("#blog-more").click(function(){
        $(".container-blog").append('<div class="page-items"><h4 class="blog-item-title"><a href="#">Рынок труда: главные цифры сентября</a></h4><p class="blog-item-category"><span>Категория: </span><a href="#">Новости</a></p><p class="blog-item-time"><span>Дата: </span>11.10.15</p><div class="blog-item-description"><img src="/img/rabota.png" alt=""><p>Осень — лучшее время для поиска сотрудников и смены работы. В это время растет не только количество вакансий, но и конкуренция среди соискателей, поэтому работодатели избирательно подходят к кандидатам. </p><p>Осенний период — это время, когда и соискатели, и работодатели наиболее активны и полезны друг</p></div></div>');
        $(".container-blog").append('<div class="page-items"><h4 class="blog-item-title"><a href="#">Рынок труда: главные цифры сентября</a></h4><p class="blog-item-category"><span>Категория: </span><a href="#">Новости</a></p><p class="blog-item-time"><span>Дата: </span>11.10.15</p><div class="blog-item-description"><img src="/img/rabota.png" alt=""><p>Осень — лучшее время для поиска сотрудников и смены работы. В это время растет не только количество вакансий, но и конкуренция среди соискателей, поэтому работодатели избирательно подходят к кандидатам. </p><p>Осенний период — это время, когда и соискатели, и работодатели наиболее активны и полезны друг</p></div></div>');
        $(".container-blog").append('<div class="page-items"><h4 class="blog-item-title"><a href="#">Рынок труда: главные цифры сентября</a></h4><p class="blog-item-category"><span>Категория: </span><a href="#">Новости</a></p><p class="blog-item-time"><span>Дата: </span>11.10.15</p><div class="blog-item-description"><img src="/img/rabota.png" alt=""><p>Осень — лучшее время для поиска сотрудников и смены работы. В это время растет не только количество вакансий, но и конкуренция среди соискателей, поэтому работодатели избирательно подходят к кандидатам. </p><p>Осенний период — это время, когда и соискатели, и работодатели наиболее активны и полезны друг</p></div></div>');
        $(".container-blog").append('<div class="page-items"><h4 class="blog-item-title"><a href="#">Рынок труда: главные цифры сентября</a></h4><p class="blog-item-category"><span>Категория: </span><a href="#">Новости</a></p><p class="blog-item-time"><span>Дата: </span>11.10.15</p><div class="blog-item-description"><img src="/img/rabota.png" alt=""><p>Осень — лучшее время для поиска сотрудников и смены работы. В это время растет не только количество вакансий, но и конкуренция среди соискателей, поэтому работодатели избирательно подходят к кандидатам. </p><p>Осенний период — это время, когда и соискатели, и работодатели наиболее активны и полезны друг</p></div></div>');
        $(".container-blog").append('<div class="page-items"><h4 class="blog-item-title"><a href="#">Рынок труда: главные цифры сентября</a></h4><p class="blog-item-category"><span>Категория: </span><a href="#">Новости</a></p><p class="blog-item-time"><span>Дата: </span>11.10.15</p><div class="blog-item-description"><img src="/img/rabota.png" alt=""><p>Осень — лучшее время для поиска сотрудников и смены работы. В это время растет не только количество вакансий, но и конкуренция среди соискателей, поэтому работодатели избирательно подходят к кандидатам. </p><p>Осенний период — это время, когда и соискатели, и работодатели наиболее активны и полезны друг</p></div></div>');
    });
    $("#vacancy-more-other").click(function(){
        $(".container-vacancy").append('<div class="page-items"><div class="item-left-part"><h4 class="item-title"><a href="#">Менеджер по туризму</a></h4><p class="company"><span>Компания:</span><a href="#">Поехали с нами</a></p><p class="item-description">Лидер в туризме — Сеть туристических агентств «Поехали с нами» проводит, на конкурсной основе, собеседования…</p><p class="city"><a href="#">Москва</a></p></div><div class="item-right-part"><h4 class="price">30 000–55 000 Р</h4><p class="time">3 минуты назад</p><a href="#" class="add-favorite"><span class="glyphicon glyphicon-bookmark"></span>В закладки</a></div></div>');
        $(".container-vacancy").append('<div class="page-items"><div class="item-left-part"><h4 class="item-title"><a href="#">Менеджер по туризму</a></h4><p class="company"><span>Компания:</span><a href="#">Поехали с нами</a></p><p class="item-description">Лидер в туризме — Сеть туристических агентств «Поехали с нами» проводит, на конкурсной основе, собеседования…</p><p class="city"><a href="#">Москва</a></p></div><div class="item-right-part"><h4 class="price">30 000–55 000 Р</h4><p class="time">3 минуты назад</p><a href="#" class="add-favorite"><span class="glyphicon glyphicon-bookmark"></span>В закладки</a></div></div>');
        $(".container-vacancy").append('<div class="page-items"><div class="item-left-part"><h4 class="item-title"><a href="#">Менеджер по туризму</a></h4><p class="company"><span>Компания:</span><a href="#">Поехали с нами</a></p><p class="item-description">Лидер в туризме — Сеть туристических агентств «Поехали с нами» проводит, на конкурсной основе, собеседования…</p><p class="city"><a href="#">Москва</a></p></div><div class="item-right-part"><h4 class="price">30 000–55 000 Р</h4><p class="time">3 минуты назад</p><a href="#" class="add-favorite"><span class="glyphicon glyphicon-bookmark"></span>В закладки</a></div></div>');
        $(".container-vacancy").append('<div class="page-items"><div class="item-left-part"><h4 class="item-title"><a href="#">Менеджер по туризму</a></h4><p class="company"><span>Компания:</span><a href="#">Поехали с нами</a></p><p class="item-description">Лидер в туризме — Сеть туристических агентств «Поехали с нами» проводит, на конкурсной основе, собеседования…</p><p class="city"><a href="#">Москва</a></p></div><div class="item-right-part"><h4 class="price">30 000–55 000 Р</h4><p class="time">3 минуты назад</p><a href="#" class="add-favorite"><span class="glyphicon glyphicon-bookmark"></span>В закладки</a></div></div>');
        $(".container-vacancy").append('<div class="page-items"><div class="item-left-part"><h4 class="item-title"><a href="#">Менеджер по туризму</a></h4><p class="company"><span>Компания:</span><a href="#">Поехали с нами</a></p><p class="item-description">Лидер в туризме — Сеть туристических агентств «Поехали с нами» проводит, на конкурсной основе, собеседования…</p><p class="city"><a href="#">Москва</a></p></div><div class="item-right-part"><h4 class="price">30 000–55 000 Р</h4><p class="time">3 минуты назад</p><a href="#" class="add-favorite"><span class="glyphicon glyphicon-bookmark"></span>В закладки</a></div></div>');
    });
});

$(window).load(function () {

    $(".loader_inner").fadeOut();
    $(".loader").delay(400).fadeOut("slow");

});

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 55.7518243, lng: 37.6161334},
        zoom: 15
    }); 
};


$(function () {

    // Do our DOM lookups beforehand
    var nav_container = $(".navbar-default");

    nav_container.waypoint({
        handler: function (direction) {
            nav_container.toggleClass('sticky', direction == "down");
            $(".content").toggleClass('fix-height', direction == "down");
            //        $("#page-vacancy .right-sidebar").toggleClass('fix-right-sidebar', direction == "down");
            //        $("#page-vacancy .col-md-pull-3").toggleClass('fix-left-content', direction == "down");
        }
    });

    $("#section2").waypoint({
        handler: function (direction) {
            $('#section3 .row_title').addClass('animated fadeInUp');
        },
        offset: -350
    });

});